/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author urwah
 */
class Manager extends Employee{
    
    double bonus;
    
    public Manager(String name, double hourlyWage, int numHours, double bonus){
       super(name,hourlyWage,numHours);
         this.bonus=bonus;
       
         System.out.println("Bonus: "+ bonus);
     
    }
    
    
    public double getBonus(){
        return this.bonus;
    }
   
    static double calculatePay(double hourlyWage, int numHours, double bonus){
    double calculatePay=  hourlyWage*numHours+bonus;
         return calculatePay;

    }
}
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author urwah
 */
 class Employee {
     String name;
     double hourlyWage;
     int numHours;
     
     
     public Employee(String name, double hourlyWage, int numHours){
         this.name=name;
         this.hourlyWage=hourlyWage;
         this.numHours=numHours;
         System.out.println("Employee: "+ name );
         System.out.println("Hourly pay: "+ hourlyWage);
         System.out.println("Working Hours: "+ numHours);
      
     }
     public String getName(){
         return this.name;
     }
     public double getHourlyWage(){
         return this.hourlyWage;
     }
     public int getNumHours(){
         return this.numHours;
     }
     static double calculatePay(double hourlyWage, int numHours){
        double calculatePay= hourlyWage*numHours;
        return calculatePay;
     }
   
}

